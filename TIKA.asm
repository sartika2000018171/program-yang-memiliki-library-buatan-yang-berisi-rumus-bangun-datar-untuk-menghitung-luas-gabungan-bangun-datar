.model tiny


.data
                 
pesan1 db 13, 10, "Masukkan angka (maksimal 5 digit) : $"
pesan2 db 13, 10, "Masukkan persen : $"
pesan3 db "% dari $"
pesan4 db " : $"
                 
string_angka db 6, ?, 6 dup (?)
string_persen db 4, ?, 4 dup (?)
string_hasil db 32, ?, 32 dup (?)
angka dw ?
persen dw ?
hasil dw ?

.code
org 100h                                                              
start:
mov ax, cs
mov ds, ax
mov es, ax

;mencetak tulisan dalam variabel pesan1
mov ah, 09h
mov dx, offset pesan1
int 21h

; menunggu inputan dan memasukkannya ke dalam variabel string_angka
mov ah, 0ah
mov dx, offset string_angka
int 21h


push bx
mov bl, string_angka[1]
add bl, 2
mov si, bx
mov string_angka[si], "$"
pop bx

;mencetak tulisan dalam variabel pesan2
mov ah, 09h
mov dx, offset pesan2
int 21h

; menunggu inputan dan memasukkannya ke dalam variabel string_persen
mov ah, 0ah
mov dx, offset string_persen
int 21h        

;menambahkan karakter $ diakhir isi variabel string_persen
push bx
mov bl, string_persen[1]
add bl, 2
mov si, bx
mov string_persen[si], "$"
pop bx        


mov bx, offset string_angka + 2
call string_ke_int
mov angka, ax

mov bx, offset string_persen + 2
call string_ke_int
mov persen, ax

mov bx, persen
mov ax, angka
mul bx

mov bx, 100
div bx

mov hasil, ax


mov bx, offset string_hasil
call int_ke_string


mov ah, 02h
mov dl, 13
int 21h

mov dl, 10
int 21h


mov ah, 09h
mov dx, offset string_persen + 2
int 21h             

mov ah, 09h
mov dx, offset pesan3
int 21h               

mov ah, 09h
mov dx, offset string_angka + 2
int 21h

mov ah, 09h
mov dx, offset pesan4
int 21h 

mov ah, 09h
mov dx, offset string_hasil
int 21h




RET




; fungsi string_ke_int
string_ke_int PROC
    push si
    push cx
    push dx    
    
    mov si, 0h
    ulang1:
    
    mov dl, bx[si]
    
    cmp dl, 0h
    je selesai1
    
    cmp dl, 24h
    je selesai1
    
    inc si
    
    jmp ulang1
    
    selesai1:
    
    cmp si, 1h
    je selesaikan_si
    jne lanjutkan
    
    selesaikan_si:
    mov ax, 0h
    mov al, bx[si - 1]
    sub al, 30h
    pop dx
    pop cx
    pop si
    
    RET 
    
    lanjutkan:
    dec si    
    push ax
    push bx
    mov ax, 0h
    mov bx, 0h
    
    mov ax, 1
    mov bx, 10
    mov cx, si
    
    dapat:
    
    mul bx
    
    
    loop dapat
    
    mov cx, ax
    pop bx
    pop ax
    
    
    mov dx, 0h
    mov ax, 0h
    mov si, 0h
    
    
    ulang:
    
    
    
    mov dl, bx[si]
    
    cmp dl, 0h
    je berhenti
    
    cmp dl, 24h
    je berhenti
    
    inc si
    sub dl, 30h
    

    push bx
    push ax
    
    mov ax, 0h
	mov dh, 0h
    mov ax, dx
    mul cx
    
    
    mov bx, ax
    
    
    pop ax
    
    add ax, bx
    
    pop bx 
    
    push ax
    mov ax, cx
    
    push bx
    
    mov bx, 10
    div bx
    
    pop bx
    
    mov cx, ax    
    pop ax
    
    
    jmp ulang
    
    berhenti:
    
    pop dx
    pop cx
    pop si    
    
RET    
    
string_ke_int ENDP



; fungsi int_ke_string
int_ke_string PROC
push dx
mov dx, 0h            
mov si, 0h

while:
    cmp ax, 0h
    je break
    
    push bx
    mov bx, 10
    div bx
    
    pop bx
    add dx, 30h
     
    mov bx[si], dx
    inc si
    mov dx, 0h
    
    jmp while
break:
         
    push si     
    mov di, si
    dec di
    mov si, 0h
    
balik:
    cmp si, di
    jge selesai
    
    mov dl, bx[si]
    mov dh, bx[di]
    
    mov bx[si], dh
    mov bx[di], dl
    
    inc si
    dec di
    

    
    jmp balik
    
    selesai:
    pop si
    pop dx
    
    push dx
    mov dl, "$"
    
    mov bx[si], dl
    pop dx
    

RET    
int_ke_string ENDP 

END start
